import { Component,ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { Storage } from '@ionic/storage';

import { IonInfiniteScroll } from '@ionic/angular';

import { HttpClientService } from '../services/http-client.service'

import { LoadingController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { PopoverComponent } from '../popover/popover.component';


@Component({
  selector: 'app-feed',
  templateUrl: './feed.page.html',
  styleUrls: ['./feed.page.scss'],
})
export class FeedPage {

  public posts;
  public data;
  public meta;
  public pages;
  public count = 1;

  private titles:any[] = [];
  private bodys:any[] = [];

  private title;
  private body;
  public newpost;

  public URL : any = 'https://gorest.co.in/public-api';
  public url;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  constructor(private router:Router,
              private storage: Storage,
              private HttpClient:HttpClientService,
              public loadingController: LoadingController,
              public popoverController: PopoverController) {   
      
    this.posts= JSON.parse(this.router.getCurrentNavigation().extras.state.posts)
    console.log("POSTS: ", (this.posts))
    this.meta = this.posts.meta;
    this.pages = this.meta.pagination.pages
    console.log("PAGES: ", this.pages)
   
    this.Data();

  }

  Data(){

    this.data = this.posts.data;
    console.log( this.data)

    //TITLES
    for ( let i=0; i< this.data.length;i++){
      let title = this.data[i].title;
      this.titles.push(title);
    }
    console.log("POSTS titles: ", this.titles)

    //OVERVIEW
    for ( let i=0; i< this.data.length;i++){
      let body = this.data[i].body;
      this.bodys.push(body);
    }
    console.log("POSTS bodys: ", this.bodys)
  }



  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      if(this.count<=this.pages){
      this.count = this.count + 1
      console.log(this.count)
          //POSTS SERVICE
          this.url = this.URL + '/posts?page=' + this.count;

          this.HttpClient.get(this.url).subscribe(
          res=>{
          console.log(res)
          this.posts = res;
          
          this.Data();

          this.presentLoading();

        },
        err=> console.log(err)
      )
      }else{
        console.log("NO THERE MORE POSTS")
      }

      if (this.data.length == 100) {
        event.target.disabled = true;
      }
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando...',
      duration: 1000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      cssClass: 'my-custom-class',
      event: ev,
      translucent: true
    });
    return await popover.present();
  }


  create(){
    this.router.navigate(['/create']);
  }

  ionViewDidEnter() {

    this.storage.get('title').then((val) => { this.title = val; })
    this.storage.get('body').then((val) => { this.body = val; })

  }


}
