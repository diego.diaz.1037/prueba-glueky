import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(private httpClient : HttpClient) { 
  }

  get(URL){
    console.log("GET: ", URL)
    return this.httpClient.get(URL);
  }

  post(DATA:any){
    console.log("POST: ", DATA) 
    let url = DATA.Url;
    let data = (DATA.Data); 
    let headers = new HttpHeaders()
    headers = headers.set("Authorization", "Bearer4741c7bdf65dfb0813329a8454d30f7c345c858d4001d");
    console.log(headers)

    return this.httpClient.post(url,data,{'headers': headers});  
  }

}
