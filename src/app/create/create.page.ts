import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage {

  public dataPost : any = {
    title: "",
    body:"",
   
  }


  constructor(private storage: Storage,
              public loadingController: LoadingController,
              public alertController: AlertController,) { 

              }


  CreatePost(){
    console.log("DATA POST:", this.dataPost)

     this.presentLoading();
     
     this.storage.set('title', this.dataPost.title);
     this.storage.set('body', this.dataPost.body);

     this.presentAlert();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Espere Por Favor...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Listo',
      subHeader: 'Post Creado',
      message: 'Puede volver al Feed para verlo',
      buttons: ['OK']
    });

    await alert.present();
  }

}
