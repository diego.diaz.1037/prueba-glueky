import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

import { Storage } from '@ionic/storage';
import { HttpClientService } from '../services/http-client.service'

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage {

///////////////////////////VARIABLES SERVICIOS

  private password_type: string = "password";
  private name_icon: string = "eye-off-outline";

  public loginData: any = {
    username: "",
    password: "",
    

  }

  public URL : any = 'https://gorest.co.in/public-api';
  public url;

  public posts;

  constructor(private router: Router,
              public loadingController: LoadingController,
              public alertController: AlertController, 
              private storage: Storage,
              private HttpClient:HttpClientService
              ) {
  }
  
  togglePasswordMode() {   
     this.password_type = this.password_type === "text" ? "password" : "text"; 
     this.name_icon = this.name_icon === "eye" ? "eye-off-outline" : "eye";
  }

  //HttpClient Angular
    Login(){

      console.log("USER LOGIN", this.loginData)

      this.storage.get('username').then((val) => {
        console.log('USERNAME:', val);
        if(this.loginData.username == val){
          this.storage.get('password').then((val) => {
            console.log('PASSWORD:', val);
            if(this.loginData.password == val){

            //USERS SERVICE
              this.url = this.URL + '/users';

                this.HttpClient.post({Url:this.url, Data:this.loginData}).subscribe(
                  res=>{
                    console.log(res)
  
                     //POSTS SERVICE
                      this.url = this.URL + '/posts';

                      this.HttpClient.get(this.url).subscribe(
                      res=>{
                      console.log(res)
                      let success:any = JSON.stringify(res)
                      this.posts = success;


                      this.router.navigate(['/feed'], {state: {posts: this.posts}});

                      this.presentLoading();

                    },
                    err=> console.log(err)
                  )
                  },
                  err=> console.log(err)                        
                )
              }else{
                this.presentAlert();
              }  
            })
          }else{
            this.presentAlert();
          }
        })
     
      
    }



    Register(){
    this.router.navigate(['/register'])
    }

    async presentLoading() {
      const loading = await this.loadingController.create({
        message: 'Cargando...',
        duration: 2000
      });
      await loading.present();
  
      const { role, data } = await loading.onDidDismiss();
      console.log('Loading dismissed!');
    }

    async presentAlert() {
      const alert = await this.alertController.create({
        header: 'Error',
        subHeader: 'Usuario no Existe',
        message: 'Verifique Usuario y contraseña',
        buttons: ['OK']
      });
  
      await alert.present();
    }



  
}
