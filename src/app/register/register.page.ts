import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage  {

  public dataUser : any = {
    username: "",
    birthdaydate:"",
    email: "",
    password:"",
   
  }

  constructor(private storage: Storage,
              public loadingController: LoadingController,
              public alertController: AlertController, ) { 

  }


  UserRegister(){
    console.log("DATA USER:", this.dataUser)

     this.presentLoading();
     
     this.storage.set('username', this.dataUser.username);
     this.storage.set('password', this.dataUser.password);

     this.presentAlert();
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Espere Por Favor...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Listo',
      subHeader: 'Registro Correcto',
      message: 'Puede iniciar sesion con su usuario',
      buttons: ['OK']
    });

    await alert.present();
  }

}
