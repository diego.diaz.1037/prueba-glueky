## PruebaGLÜKY

This project was generated with [Ionic CLI](https://github.com/ionic-team/ionic-cli) version 5.0.0.


# Installation Ionic CLI 

Run command of instalation
`npm install -g @ionic/cli` The Ionic command line interface (CLI) is the go-to tool for developing Ionic apps.


# Modules and Dependencies

Run command in project directory
`npm i` this will generate the installation of the Node.JS modules required in the project, in addition to the Cordova dependencies necessary for its operation.


# Development server

Run comand of launch 
`ionic serve` for run the aplication a dev server. Navigate to `http://localhost:8100/`. The app will automatically reload if you change any of the source files.
